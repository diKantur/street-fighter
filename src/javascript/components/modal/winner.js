import { showModal } from "./modal";
import { createFighterImage } from '../fighterPreview';
import { fighterDetailsMap } from "../fighterSelector";
import { createElement } from "../../helpers/domHelper";
import App from "../../app";

export function showWinnerModal(fighter) {
  const bodyElement = createElement({
    tagName: 'div',
    className: 'modal-body'
  });

  bodyElement.append(createFighterImage(fighter));
  
  showModal({
    title: `${fighter.name} Win!`,
    bodyElement,
    onClose: () => {
      const root = document.getElementById('root');
      root.innerHTML = '';
      fighterDetailsMap.clear();
      new App()
    }
  })
}