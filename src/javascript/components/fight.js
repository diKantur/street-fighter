import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const firstPlayer = {
    ...firstFighter,
    totalHealth: firstFighter.health,
    indicator: document.getElementById('left-fighter-indicator'),
    timeOver: true,
  };

  const secondPlayer = {
    ...secondFighter,
    totalHealth: secondFighter.health,
    indicator: document.getElementById('right-fighter-indicator'),
    timeOver: true,
  };

  const timeCount = 10000;

  return new Promise((resolve) => {
    function critOnKeys(attacker, defender, ...codes) {
      const pressed = new Set();

      document.addEventListener('keydown', function (event) {
        pressed.add(event.code);

        for (let code of codes) {
          if (!pressed.has(code)) {
            return;
          }
        }
        
        if (attacker.timeOver) {
          attacker.timeOver = false;
          setDamage(attacker, defender, true);
          setTimeout(() => {
            attacker.timeOver = true;
          }, timeCount);
        }
        
        pressed.clear();
      });

      document.addEventListener('keyup', function (event) {
        pressed.delete(event.code);
      });
    }

    function fightOnKeys(...codes) {
      const pressed = {
        val: new Set(),
        isPressed (one, two) {
          return (this.val.has(controls[one]) || this.val.has(controls[two]));
        }
      };

      document.addEventListener('keydown', function (event) {
        pressed.val.add(event.code);

        const isBlocked = pressed.isPressed('PlayerOneBlock', 'PlayerTwoBlock');
        const isAttacked = pressed.isPressed('PlayerOneAttack', 'PlayerTwoAttack');

        switch (true) {
          case !pressed.val.has(codes[0]) || isBlocked:
            return;
          case isAttacked:
            attackAndDefense(pressed.val);
            return;
        }

        pressed.val.clear();
      });

      document.addEventListener('keyup', function (event) {
        pressed.val.delete(event.code);
      });

    }

    function attackAndDefense(pressed) {
      let attacker, defender;
      switch (true) {
        case pressed.has(controls.PlayerOneAttack):
          attacker = firstPlayer;
          defender = secondPlayer;
          break;
        case pressed.has(controls.PlayerTwoAttack):
          attacker = secondPlayer;
          defender = firstPlayer;
          break;
      }
      setDamage(attacker, defender);
    }

    function setDamage(attacker, defender, crit) {
      defender.health -= crit ? 
      getCriticalDamage(attacker) : 
      getDamage(attacker, defender);

      if (defender.health <= 0) {
        defender.health = 0;
        resolve(attacker);
      }
      
      setHealth(defender);
    }

    fightOnKeys(controls.PlayerOneBlock);
    fightOnKeys(controls.PlayerTwoBlock);
    fightOnKeys(controls.PlayerTwoAttack);
    fightOnKeys(controls.PlayerOneAttack);

    critOnKeys(firstPlayer, secondPlayer, ...controls.PlayerOneCriticalHitCombination);
    critOnKeys(secondPlayer, firstPlayer, ...controls.PlayerTwoCriticalHitCombination);
  });
}

function setHealth(defender) {
  defender.indicator.style.width = `${(defender.health / defender.totalHealth) * 100}%`;
}

function getCriticalDamage(fighter) {
  return fighter.attack * 2;
}

export function getDamage(attacker, defender) {
  let hit = getHitPower(attacker);
  let block = getBlockPower(defender);
  return hit > block ? hit - block : 0;
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1);
}
